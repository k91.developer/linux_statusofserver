while :
do
	cat T1 > TT
	sar 1 1 | grep Average|awk '{print "CPU_FREE="$8";"}' >> TT
	sar 1 1 | grep Average|awk '{print "CPU_USED="100-$8";"}' >> TT
	free |grep Mem|awk '{print "MEM_FREE="$4";"}' >> TT
	free |grep Mem|awk '{print "MEM_USED="$3";"}' >> TT
	df -k|grep -v Filesystem|awk '{sum += $4} END {print "DSK_FREE="sum";"}' >> TT
	df -k|grep -v Filesystem|awk '{sum += $3} END {print "DSK_USED="sum";"}' >> TT
	cat T2 >> TT
	cp TT serverstatus.html
done

